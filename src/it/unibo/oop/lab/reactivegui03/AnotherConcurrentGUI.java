package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * creates an updated version of the previous GUI.
 */
public final class AnotherConcurrentGUI extends JFrame {
	
	private static final long serialVersionUID = 64934994572016353L;
	private static final double WIDTH_PERC = 0.2;
	private static final double HEIGHT_PERC = 0.1;
	private static final int DECS_TIMEOUT = 100;
	private final JLabel display = new JLabel();
	private final JButton up = new JButton("up");
	private final JButton down = new JButton("down");
	private final JButton stop = new JButton("stop");

    public AnotherConcurrentGUI(){
    	super();
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		final JPanel panel = new JPanel();
		panel.add(display);
		panel.add(up);
		panel.add(down);
		panel.add(stop);
		this.getContentPane().add(panel);
		this.setVisible(true);

        /*
		 * Create the counter agent and start it. This is actually not so good:
		 * thread management should be left to
		 * java.util.concurrent.ExecutorService
		 */
		final Agent agent = new Agent();
		new Thread(agent).start();
		/*
		 * Register a listener that stops it
		 */
		stop.addActionListener(new ActionListener() {
			/**
			 * event handler associated to action event on button stop.
			 * 
			 * @param e
			 *            the action event that will be handled by this listener
			 */
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Agent should be final
				agent.stopCounting();
				up.setEnabled(false);
				down.setEnabled(false);
				stop.setEnabled(false);
			}
		});

		/*
		 * Register a listener that sets increasing counting
		 */
		up.addActionListener(new ActionListener() {
			/**
			 * event handler associated to action event on button up.
			 * 
			 * @param e
			 *            the action event that will be handled by this listener
			 */
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Agent should be final
				agent.increase();
			}
		});

		/*
		 * Register a listener that seats decreasing counting
		 */
		down.addActionListener(new ActionListener() {
			/**
			 * event handler associated to action event on button down.
			 * 
			 * @param e
			 *            the action event that will be handled by this listener
			 */
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Agent should be final
				agent.decrease();
			}
		});
	}
    
    /*
	 * The counter agent is implemented as a nested class. This makes it
	 * invisible outside and encapsulated.
	 */
	private class Agent implements Runnable {
		/*
		 * stop is volatile to ensure ordered access
		 */
		private volatile boolean stop;
		private volatile boolean increasing = true;  /* "false" if decreasing */
		private int counter;

		public void run() {
			int decs=0;
			
			while (!this.stop) {
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
						}
					});

					if (increasing) {
						this.counter++;
					} else {
						this.counter--;
					}
					if (++decs >= DECS_TIMEOUT) { this.stop=true; }
					Thread.sleep(100);
				} catch (InvocationTargetException | InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}

		/**
		 * External command to stop counting.
		 */
		public void stopCounting() {
			this.stop = true;
		}

		/**
		 * External command to revert the counting method to increasing.
		 */
		public void increase() {
			this.increasing = true;
		}

		/**
		 * External command to revert the counting method to decreasing.
		 */
		public void decrease() {
			this.increasing = false;
		}
	}
}

